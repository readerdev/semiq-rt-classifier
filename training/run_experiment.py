"""Experiment-running framework."""
import os
import random
import argparse
import importlib
from pathlib import Path
import datetime

import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
from tensorflow.keras.callbacks import Callback, ModelCheckpoint

from classifier.data.data_pipeliner import DataPipeliner, DataAugmentor


AUTOTUNE = tf.data.AUTOTUNE


def configure_dataset(data_pipeliner, args):
    """Batch, shuffle, and optimize dataset."""
    ds = data_pipeliner.dataset
    ds = ds.cache().shuffle(buffer_size=data_pipeliner.len, reshuffle_each_iteration=True)
    ds = ds.batch(args.batch_size).prefetch(buffer_size=AUTOTUNE)
    if data_pipeliner.training and args.augment:
        augmentor = DataAugmentor(ds, strength=args.strength)
        return augmentor.apply()
    return ds


def import_class(module_and_class_name):
    """Import class from a module, e.g., classifier.models.CNN."""
    module_name, class_name = module_and_class_name.rsplit(".", 1)
    module = importlib.import_module(module_name)
    class_ = getattr(module, class_name)
    return class_


def get_callbacks(train, test, args):
    """Get training, eval, and ckpt callbacks."""
    now = datetime.datetime.now().strftime("%d%m%Y-%H%M%S")

    scalars_dir = os.path.join('logs', 'scalars', now)
    tensorboard_callback = [tf.keras.callbacks.TensorBoard(log_dir=scalars_dir, histogram_freq=1)]

    checkpoint_dir = os.path.join('logs', 'checkpoints', now)
    filepath = os.path.join(checkpoint_dir, '{epoch:03d}')
    checkpoint_callback = [ModelCheckpoint(filepath,
                                        monitor='val_accuracy',
                                        mode='max',
                                        save_best_only=True,
                                        save_weights_only=False,
                                        verbose=0)]
    callbacks = [tensorboard_callback]
    if args.save_model:
        callbacks += checkpoint_callback
    return callbacks


def do_train(model, train_ds, test_ds, args):
    """Call model.fit() with custom callbacks."""
    callbacks = get_callbacks(train_ds, test_ds, args)
    model.fit(
        train_ds,
        epochs=args.epochs,
        callbacks=callbacks,
        validation_data=test_ds,
        verbose=2,
    )
  

def get_args():
    """Return command-line args."""
    parser = argparse.ArgumentParser(
        description='Command line interface for training/evaluating a rapidtest classifier')
    # Hyperparams
    parser.add_argument(
        '--model_class', help='Model architecture', default='CNN')
    parser.add_argument(
        '--batch_size', help='Batch size', type=int, default=8)
    parser.add_argument(
        '--lr', help='Learning rate', type=float, default=0.0001)
    parser.add_argument(
        '--epochs', help='Number of training epochs', type=int, default=10)
    parser.add_argument(
        '--input_size', help='Size of input layer', type=int, default=96)
    parser.add_argument(
        '--augment', help='Augment training images', action='store_true', default=False)
    parser.add_argument(
        '--strength', help='Strength of color distortions', type=float, default=0.125)
    # Configuration
    parser.add_argument(
        '--images_dir', help='Path to images directory', type=str, default=None)
    parser.add_argument(
        '--full_train_set', help='Combine training and dev folds', action='store_true', default=False)
    parser.add_argument(
        '--save_model', help='Save (full) model during training', action='store_true', default=False)
    # Tasks
    parser.add_argument(
        '--train', help='Do training and evaluation loop', action='store_true', default=False)
    parser.add_argument(
        '--cross_val', help='Perform cross validation', action='store_true', default=False)
    # Model-specific args/hyperparams
    temp_args, _ = parser.parse_known_args()
    model_class = import_class(f"classifier.models.{temp_args.model_class}")
    model_class.add_to_argparse(parser)

    args = parser.parse_args()
    return args


def main(args):
    """Orchestrate experiment."""
    train_pipeliner = DataPipeliner(
        img_dir=args.images_dir,
        folds=args.folds['train'],
        input_size=args.input_size,
        training=True
    )
    val_pipeliner = DataPipeliner(
        img_dir=args.images_dir,
        folds=args.folds['dev'],
        input_size=args.input_size
    )

    train_ds = configure_dataset(train_pipeliner, args)
    val_ds = configure_dataset(val_pipeliner, args)

    if args.full_train_set:
        train_pipeliner = DataPipeliner(
            img_dir=args.images_dir,
            folds=args.folds['train'] + args.folds['dev'],
            input_size=args.input_size,
            training=True
        )
        val_pipeliner = DataPipeliner(
            img_dir=args.images_dir,
            folds=args.folds['test'],
            input_size=args.input_size
        )

        train_ds = configure_dataset(train_pipeliner, args)
        val_ds = configure_dataset(val_pipeliner, args)

    model_class = import_class(f"classifier.models.{args.model_class}")
    model = model_class(args)

    optimizer = tf.keras.optimizers.Adam(learning_rate=args.lr)
    loss_fn = tf.losses.SparseCategoricalCrossentropy(from_logits=True) 
    model.compile(optimizer=optimizer, loss=loss_fn, metrics='accuracy')

    if args.train:
        do_train(model, train_ds, val_ds, args)


if __name__ == "__main__":

    args = get_args()
    tf.random.set_seed(0)
    np.random.seed(0)
    test_fold = 3

    if args.cross_val:
        num_folds = 4
        for dev_fold in range(num_folds - 1):
            train_folds = [i for i in range(num_folds - 1) if i != dev_fold]
            args.folds = {'train': train_folds, 'dev': [dev_fold], 'test': [test_fold]}
            main(args)
    else:
        args.folds = {'train': [0, 2], 'dev': [1], 'test': [test_fold]}
        main(args)