"""Custom TensorFlow data pipeline."""
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
import argparse

import tensorflow as tf
import matplotlib.pyplot as plt


AUTOTUNE = tf.data.AUTOTUNE
_SPLIT_SEED = 123


class DataPipeliner:
    """Build a tf.data.Dataset from an image directory."""

    def __init__(self, img_dir, input_size=96, folds=None, file_pattern='/*/*.png', training=False):
        self.img_height, self.img_width = input_size, input_size
        self.folds = folds
        self.training = training
        self.files = tf.data.Dataset.list_files(
            file_pattern=(img_dir + file_pattern),
            shuffle=True,
            seed=_SPLIT_SEED
        )
        if self.folds is not None:
            self.files = self._get_subset(self.files)
        self.dataset = self._transform(self.files)
        self.len = len(self.dataset)

    def _get_subset(self, files_ds):
        fold_size = int(len(files_ds) * 0.25)

        remaining = files_ds
        files_in_folds = []
        for i in range(4):
            if len(files_ds) % 2 == 1:
                if i == 3:
                    fold_size += 1
            fold = remaining.take(fold_size)
            remaining = remaining.skip(fold_size)
            files_in_folds.append(fold)

        subset = files_in_folds[self.folds[0]]
        for fold_idx in self.folds[1:]:
            subset = subset.concatenate(files_in_folds[fold_idx])
        return subset

    def _transform(self, files_ds):
        processed = files_ds.map(self._process_path, num_parallel_calls=AUTOTUNE)
        processed = processed.map(self._process_pairs, num_parallel_calls=AUTOTUNE)
        return processed.map(self._rescale, num_parallel_calls=AUTOTUNE)


    def _process_path(self, file_path):
        img = tf.io.read_file(file_path)
        img = tf.image.decode_jpeg(img, channels=3)
        img = tf.image.resize(img, [self.img_height, self.img_width])
        label = self._get_label(file_path)
        return img, label

    def _get_label(self, file_path):
        label = tf.strings.split(file_path, sep=os.path.sep)[-2]
        return label

    def _process_pairs(self, img, label):
        label = tf.py_function(self._encode_label_as_int, [label], tf.int32)
        label = tf.ensure_shape(label, shape=())
        return img, label

    def _encode_label_as_int(self, label):
        label_dict = {
            'negative': 0, 
            'low': 1,
            'medium': 2,
            'high': 3
        }
        label = label.numpy().decode("utf-8")
        return label_dict[label]

    def _rescale(self, img, label):
        return img / 255., label


class DataAugmentor:
    """Construct a SimCLR-inspired data augmentation pipeline."""

    def __init__(self, dataset, strength=0.125):
        self.dataset = dataset
        self.strength = strength

    def _get_augmentation_pipeline(self):
        return tf.keras.Sequential([
            tf.keras.layers.Lambda(_CustomAugment(self.strength)),
        ])

    def apply(self):
        """Apply augmentation pipeline to dataset."""
        augmentation = self._get_augmentation_pipeline()
        aug_dataset = self.dataset.map(lambda x, y: (augmentation(x), y), num_parallel_calls=AUTOTUNE)
        return aug_dataset


class _CustomAugment:
    def __init__(self, strength):
        self.s = strength

    def __call__(self, image):
        image = self._random_apply(self._color_jitter, image, p=0.8)
        image = self._random_apply(self._flip, image, p=0.8)
        return image

    def _flip(self, x):
        return tf.image.random_flip_left_right(x)

    def _color_jitter(self, x):
        x = tf.image.random_brightness(x, max_delta=0.8 * self.s)
        x = tf.image.random_contrast(x, lower=(1 - 0.8) * self.s, upper=(1 + 0.8) * self.s)
        x = tf.image.random_saturation(x, lower=(1 - 0.8) * self.s, upper=(1 + 0.8) * self.s)
        x = tf.image.random_hue(x, max_delta=0.2 * self.s)
        x = tf.clip_by_value(x, 0, 1)
        return x

    def _random_apply(self, func, x, p):
        return tf.cond(
            tf.less(tf.random.uniform([], minval=0, maxval=1, dtype=tf.float32),
                    tf.cast(p, tf.float32)),
            lambda: func(x),
            lambda: x)


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Custom input pipeline')
    parser.add_argument('-i', help='path to images', type=str)
    args = parser.parse_args()

    rapidtest_dataset = DataPipeliner(args.i, folds=[0])
    dataset = rapidtest_dataset.dataset

    plt.figure(figsize=(10, 10))
    for i, (img, label) in enumerate(dataset.take(9)):
        img *= 255
        ax = plt.subplot(3, 3, i + 1)
        plt.imshow(img.numpy().astype("uint8"))
        plt.title(label.numpy())
        plt.axis("off")
    plt.show()