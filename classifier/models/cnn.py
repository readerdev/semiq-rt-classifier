"""Google Brain CBR model and building blocks."""
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

import tensorflow as tf
from tensorflow.keras import layers
from tensorflow.keras.models import Sequential


CONV1_DIM = 64
CONV2_DIM = 128
CONV3_DIM = 256
CONV4_DIM = 512
FILTER_SIZE = 5


class CBRBlock(layers.Layer):
    """
    A convolutional block.

    CONV -> BN -> RELU
    """

    def __init__(self, out_channels, kernel_size=3):
        super(CBRBlock, self).__init__()
        self.conv = layers.Conv2D(out_channels, kernel_size, padding='same')
        self.bn = layers.BatchNormalization()

    def call(self, x, training=False):
        """Do layer forward pass."""
        x = self.conv(x)
        x = self.bn(x, training=training)
        x = tf.nn.relu(x)
        return x

class CNN(tf.keras.Model):
    """
    A lightweight conv2d-batchnorm-relu CNN.

    [CONV -> RELU -> BN -> POOL]*N -> POOL -> FC
 
    See: https://arxiv.org/pdf/1902.07208.pdf
    """

    def __init__(self, args=None):
        super(CNN, self).__init__()

        self.args = vars(args) if args is not None else {}

        conv1_dim = self.args.get("conv1_dim", CONV1_DIM)
        conv2_dim = self.args.get("conv2_dim", CONV2_DIM)
        conv3_dim = self.args.get("conv3_dim", CONV3_DIM)
        conv4_dim = self.args.get("conv4_dim", CONV4_DIM)
        filter_size = self.args.get("filter_size", FILTER_SIZE)

        self.conv1 = CBRBlock(conv1_dim, filter_size)
        self.conv2 = CBRBlock(conv2_dim, filter_size)
        self.conv3 = CBRBlock(conv3_dim, filter_size)
        self.conv4 = CBRBlock(conv4_dim, filter_size)
        self.max_pool = layers.MaxPooling2D(3, 2)
        self.global_pool = tf.keras.layers.GlobalAveragePooling2D()
        self.fc1 = layers.Dense(4)


    def call(self, x, training=False):
        """Do model forward pass."""
        x = self.conv1(x, training=training)
        x = self.max_pool(x)
        x = self.conv2(x, training=training)
        x = self.max_pool(x)
        x = self.conv3(x, training=training)
        x = self.max_pool(x)
        x = self.conv4(x, training=training)
        x = self.max_pool(x)
        x = self.global_pool(x)
        x = self.fc1(x)
        return x
    

    @staticmethod
    def add_to_argparse(parser):
        """Log customizable params."""
        parser.add_argument("--conv1_dim", type=int, default=CONV1_DIM)
        parser.add_argument("--conv2_dim", type=int, default=CONV2_DIM)
        parser.add_argument("--conv3_dim", type=int, default=CONV3_DIM)
        parser.add_argument("--conv4_dim", type=int, default=CONV4_DIM)
        parser.add_argument("--filter_size", type=int, default=FILTER_SIZE)
        return parser


def inspect_layers(x, model):
    """Inspect layer input shape changes."""
    for layer in model.layers:
        x = layer(x)
        print(layer.__class__.__name__, 'output shape:\t', x.shape)


if __name__ == "__main__":

    x = tf.random.uniform((1, 96, 96, 3), minval=0., maxval=1.)
    model = CNN()
    y = model(x)
    inspect_layers(x, model)
